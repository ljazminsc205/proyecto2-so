#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <strings.h>
#include "stackProcess.c" 


pthread_t threadSpy;
pthread_t threadReader;
pthread_t threadReaderSelfish;
pthread_t threadWriter;

int contPID = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
typedef struct mensaje
{
    int cant;
    int t_work;
    int t_sleep;
    char type;
}mensaje;

struct PCB *procesos = NULL;

//base para el programa espía, en proceso
// void spy(){
//     printf( "Soy un spy %i\n" , 5);
// };

void *funcReader  (void * mensaje){
     struct mensaje * datos = (struct mensaje *) mensaje;
     int t_sleep = datos->t_sleep;
     int t_work = datos->t_work;

     pthread_mutex_lock(&lock);

     struct PCB *datosPCB = (struct PCB *) malloc(sizeof(struct PCB));
     datosPCB->pid= contPID;
     datosPCB->type = 0;
     datosPCB->ifile = 0;
     datosPCB->t_work=t_work;
     datosPCB->t_sleep=t_sleep;
     
     pushProcessEnd(&procesos, contPID , datosPCB->type  , datosPCB->ifile,  0, datosPCB->t_work ,  datosPCB->t_sleep);
     contPID += 1;
     pthread_mutex_unlock(&lock);

};

void *funcReaderSelfish (void * mensaje){
     struct mensaje * datos = (struct mensaje *) mensaje;
     int t_sleep = datos->t_sleep;
     int t_work = datos->t_work;

     pthread_mutex_lock(&lock);
     struct PCB *datosPCB = (struct PCB *) malloc(sizeof(struct PCB));
     datosPCB->pid= contPID;
     datosPCB->type = 1;
     datosPCB->ifile = 0;
     datosPCB->t_work=t_work;
     datosPCB->t_sleep=t_sleep;

     pushProcessEnd(&procesos, contPID , datosPCB->type  , datosPCB->ifile,  0, datosPCB->t_work ,  datosPCB->t_sleep);
     contPID += 1;
     pthread_mutex_unlock(&lock);
};

void *funcWriter(void * mensaje){
     struct mensaje * datos = (struct mensaje *) mensaje;
     int t_sleep = datos->t_sleep;
     int t_work = datos->t_work;

     pthread_mutex_lock(&lock);
     struct PCB *datosPCB = (struct PCB *) malloc(sizeof(struct PCB));
     datosPCB->pid= contPID;
     datosPCB->type = 2;
     datosPCB->ifile = 0;
     datosPCB->t_work=t_work;
     datosPCB->t_sleep=t_sleep;

     pushProcessEnd(&procesos, contPID , datosPCB->type  , datosPCB->ifile,  0, datosPCB->t_work ,  datosPCB->t_sleep);
     contPID += 1;
     pthread_mutex_unlock(&lock);
};


void * createThread(void * mensaje){
    struct mensaje * datos = (struct mensaje *) mensaje;
    char type = datos->type;
    int cant = datos->cant;
    pthread_t thread;

     if (type=='R')
     {
         for (size_t i = 0; i < cant; i++)
         {
             pthread_create(&thread, NULL, funcReader, (void *) datos);
         };
     }
     else if (type=='S')
     {
         for (size_t i = 0; i < cant; i++)
         {
             pthread_create(&thread, NULL, funcReaderSelfish, (void *) datos);
         };
     }
     else
     {
         for (size_t i = 0; i < cant; i++)
         {
             pthread_create(&thread, NULL, funcWriter, (void *) datos);
         };
     }
    pthread_join(thread, NULL);
};

int main (int argc, char *argv[]){
    int cant;
    int t_sleep;
    int t_work ;
    struct mensaje *writters = (struct mensaje *) malloc(sizeof(struct mensaje));;
    struct mensaje *readers = (struct mensaje *) malloc(sizeof(struct mensaje));;
    struct mensaje *readersSelfish = (struct mensaje *) malloc(sizeof(struct mensaje));

    printf ("Escritores\nIngrese la cantidad de escritores ");
    scanf("%i",&cant);
    printf ("Ingrese el tiempo que duermen ");
    scanf("%i",&t_sleep);
    printf ("Ingrese el tiempo que escriben ");
    scanf("%i",&t_work);

    writters->cant=cant;
    writters->t_work=t_work;
    writters->t_sleep=t_sleep;
    writters->type= 'W';

    printf ("\nLectores\nIngrese la cantidad de lectores ");
    scanf("%i",&cant);
    printf ("Ingrese el tiempo que duermen ");
    scanf("%i",&t_sleep);
    printf ("Ingrese el tiempo que leen ");
    scanf("%i",&t_work);

    readers->cant=cant;
    readers->t_work=t_work;
    readers->t_sleep=t_sleep;
    readers->type= 'R';
    
    printf ("\nLectores egoístas\nIngrese la cantidad de lectores egoístas ");
    scanf("%i",&cant);
    printf ("Ingrese el tiempo que duermen ");
    scanf("%i",&t_sleep);
    printf ("Ingrese el tiempo que leen ");
    scanf("%i",&t_work);

    readersSelfish->cant = cant;
    readersSelfish->t_work = t_work;
    readersSelfish->t_sleep = t_sleep;
    readersSelfish->type = 'S';


    pthread_create(&threadReader, NULL, createThread, (void *) readersSelfish);
    pthread_create(&threadReaderSelfish, NULL, createThread, (void *) readers);
    pthread_create(&threadWriter, NULL, createThread, (void *) writters);

    pthread_join(threadReader, NULL);
    pthread_join(threadReaderSelfish, NULL);
    pthread_join(threadWriter, NULL);
    printProcess(procesos);
    return 0;

};
