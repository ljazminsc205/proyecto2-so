all : writer reader readerS inicia final spy

writer : factoryWriter.c
	gcc -pthread -o fwriter factoryWriter.c

reader : factoryReaders.c
	gcc -pthread -o freader factoryReaders.c

readerS : factoryReadersSelfish.c
	gcc -pthread -o freaderS factoryReadersSelfish.c

inicia : inicializador.c mensaje.h
	gcc -o inicia inicializador.c

final : finalizador.c
	gcc -o final finalizador.c

spy : spy.c
	gcc -pthread -o spy spy.c

clean : 
	rm fwriter freader freaderS inicia final spy
