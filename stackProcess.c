#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct PCB
{
    int pid;
    int type;
    int ifile;
    int state;
    int t_work;
    int t_sleep;
    struct PCB * after;
}PCB;

void pushProcessEnd(struct PCB **refActual , int contPID , int type , int ifile, int state, int t_work , int t_sleep){

    struct PCB *nuevoNodo = (struct PCB *)malloc(sizeof(struct PCB));
    nuevoNodo->pid = contPID;
    nuevoNodo->type = type;
	nuevoNodo->ifile = ifile;
	nuevoNodo->state = state;
    nuevoNodo->t_work = t_work;
	nuevoNodo->t_sleep = t_sleep;
    nuevoNodo->after = NULL;
    
    struct PCB * head = *refActual;

    if(head == NULL){
        *refActual = nuevoNodo;
        return;
    }

    while (head->after != NULL)
    {
        head=head->after;
    }
    head->after = nuevoNodo;
};

void printProcess(struct PCB * procesos){
    struct PCB * actual = procesos;
    while (actual!= NULL)
    {
        printf("PID: %i Type: %i ifile: %i state: %i Work: %i Sleep: %i\n" , actual->pid , actual->type , actual->ifile, actual->state , actual->t_work , actual->t_sleep);
        actual= actual->after;
    }
};

void updateProcess(int id, int ifile, int state, struct PCB * procesos){
struct PCB * actual = procesos;
    while (actual!= NULL)
    {
        if (actual->pid ==id)
        {
            actual->ifile = ifile;
            actual->state = state;
            //printf("PID: %i Type: %i ifile: %i state: %i Work: %i Sleep: %i\n",actual->pid , actual->type , actual->ifile, actual->state , actual->t_work , actual->t_sleep);    
        }
        actual= actual->after;
    }
};

char * concatProcess(struct PCB * procesos){
    struct PCB * actual = procesos;
    char val[3];
    char str[80];
    char mensaje[20000];
    strcpy(mensaje, "");
    while (actual!= NULL)
    {

        strcpy(str, "these ");
        strcat(str, "strings ");
        strcat(str, "are ");
        strcat(str, "concatenated.");

        strcat(mensaje, " PID: ");  
        sprintf(val, "%d", actual->pid);
        strcat(mensaje, val);

        strcat(mensaje, " Type: ");  
        sprintf(val, "%d", actual->type);
        strcat(mensaje, val);

        strcat(mensaje, " State: ");  
        sprintf(val, "%d", actual->state);
        strcat(mensaje, val); 

        strcat(mensaje, " iFile: ");  
        sprintf(val, "%d", actual->ifile);
        strcat(mensaje, val);  

        strcat(mensaje, " Work: ");  
        sprintf(val, "%d", actual->t_work);
        strcat(mensaje, val);  

        strcat(mensaje, " Sleep: ");  
        sprintf(val, "%d", actual->t_sleep);
        strcat(mensaje, val);  

        strcat(mensaje, "\n");
        actual= actual->after;
    }
    char * msj = (char *) malloc(20000);
    strcpy(msj, mensaje);
    return msj;
};
