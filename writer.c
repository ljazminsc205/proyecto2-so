#include "svshm_string.h"
#include "mensaje.h"
#include <semaphore.h>

int main(int argc, char *argv[]) {
	
	int shmid, size_shmid, semid;
	struct Mensaje *lista;
	int *size;
	struct sembuf p[1] ;//= {0, -1, 0};
    p[0].sem_num = 0;        /* Operate on semaphore 0 */
    p[0].sem_op = -1;         /* Increment value by one */
    p[0].sem_flg = 0;
	struct sembuf v[1] ;
	v[0].sem_num = 0;        /* Operate on semaphore 0 */
	v[0].sem_op = +1;         /* Decrement value by one */
	v[0].sem_flg = 0;

	/* consigue el id de la memoria compartida 
	 * con permisos de read y write */
	shmid = shmget(KEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");

	// asigna lista a la memoria compartida
	lista = (struct Mensaje *)shmat(shmid, NULL, 0);
	if (lista == (struct Mensaje *) -1)
	errExit("shmat");

	// obtener el tamano de la lista 
	size_shmid = shmget(SKEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");
	
	size = shmat(size_shmid, NULL, 0);
	if (size == (void *) -1)
	errExit("shmat");

	// obtener el id del semaforo
	semid = semget(SEM_KEY, 1, 0666);
    if (shmid == -1)
    errExit("shmget");

	// /* Decrement semaphore to 0 */
    if (semop(semid, p, 1) == -1)
    	errExit("semop");
	//-- región critica
	int i;
	for (i=0; i<*size; i++) {
		strcpy(lista[i].data,"hola");	
		printf("escribi en: %i\n", lista[i].key);
	}
	//-- end region critica
	/* Increment semaphore to 0 */
	 if (semop(semid, v, 1) == -1)
	 	errExit("semop");
	
	sleep(4);
	return 0;
}
