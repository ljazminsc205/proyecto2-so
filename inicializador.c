#include "svshm_string.h"
#include "mensaje.h"

int main(int argc, char *argv[]) {

	int shmid, semid, cant, size_shmid;
	int *size;
	struct Mensaje *lista;
	union semun arg, dummy;

	// comprobar que haya un argumento 
	if (argc != 2) {
		fprintf(stderr, "Uso: %s cantLineas\n", argv[0]);
		exit(EXIT_FAILURE);
    }

	cant = atoi(argv[1]);	

	/* solicitar el id de la memoria compartida
	 * IPC_CREAT | 0666 lo que hace es solicitarla y en caso
	 * de que ya exista solo consigue el shmid con los 
	 * permisos 0666 de la forma que lo utiliza Linux 
	 * es decir -rw-rw-rw , read y write para todos. */
	shmid = shmget(KEY, MEM_SIZE, IPC_CREAT | 0666);
	if (shmid == -1)
	errExit("shmget");

	// asigna lista a la memoria compartida
	lista = shmat(shmid, NULL, 0);
	if (lista == (void *) -1)
	errExit("shmat");

	// inicializar la lista con la cantidad asignada
	int i;
	for (i=0; i<cant; i++) {
		lista[i].key=i;
		strcpy(lista[i].data,"");
	}
	
	// crear un semaforo nuevo 
	semid = semget(SEM_KEY, 2, IPC_CREAT | 0666);
    if (shmid == -1)
    errExit("shmget");
	
	// inicializar el semaforo en 0
	arg.val = 1;
	if (semctl(semid, 0, SETVAL, arg) == -1)
	errExit("semctl");
	
	// inicializar el semaforo en 0
	arg.val = 0;
	if (semctl(semid, 1, SETVAL, arg) == -1)
	errExit("semctl");

	// compartir el tamano de la lista 
	size_shmid = shmget(SKEY, MEM_SIZE, IPC_CREAT | 0666);
	if (shmid == -1)
	errExit("shmget");
	
	size = shmat(size_shmid, NULL, 0);
	if (size == (void *) -1)
	errExit("shmat");

	memcpy(size, &cant, sizeof(cant));

	return 0;
}
