#include "svshm_string.h"


int main(int argc, char *argv[]) {

	int shmid, semid, size_shmid;
	union semun arg, dummy;

	/* consigue el id de la memoria compartida 
	 * con permisos de read y write */
	shmid = shmget(KEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");
	
	// obtener el id de la memoria de tamano 
	size_shmid = shmget(SKEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");

	// obtener el id del semaforo
	semid = semget(SEM_KEY, 2, 0666);
    if (shmid == -1)
    errExit("shmget");


	// elimina la memoria compartida
	if (shmctl(shmid, IPC_RMID, NULL) == -1)
    errExit("shmctl");

	// elimina la memoria de tamano
	if (shmctl(size_shmid, IPC_RMID, NULL) == -1)
    errExit("shmctl");

	// elimina el semaforo
	if (semctl(semid, 0, IPC_RMID, dummy) == -1)
        errExit("semctl"); 


	return 0;
}
