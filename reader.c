#include "svshm_string.h"
#include "mensaje.h"

int main(int argc, char *argv[]) {

	int shmid, size_shmid, semid;
	struct Mensaje *lista;
	int *size;
	struct sembuf sop;

	/* consigue el id de la memoria compartida 
	 * con permisos de read y write */
	shmid = shmget(KEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");

	// asigna lista a la memoria compartida
	lista = (struct Mensaje *)shmat(shmid, NULL, 0);
	if (lista == (struct Mensaje *) -1)
	errExit("shmat");

	// obtener el tamano de la lista 
	size_shmid = shmget(SKEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");
	
	size = shmat(size_shmid, NULL, 0);
	if (size == (void *) -1)
	errExit("shmat");

	// obtener el id del semaforo
	semid = semget(SEM_KEY, 2, 0666);
    if (shmid == -1)
    errExit("shmget");

	// mostrar la lista 
	int i;
	for (i=0; i<*size; i++) {
		/* Wait for semaphore 0 value to become 0 */
		/*sop.sem_num = 1;
		sop.sem_op = 0;
		sop.sem_flg = 0;
		if (semop(semid, &sop, 1) == -1)
		errExit("semop");
*/
		printf("%i ", lista[i].key);
		printf("data: %.*s\n", (int) sizeof(lista[i].data), lista[i].data);
		sleep(4);
	}

	// decrementar el semaforo 1 antes de entrar
	//sop.sem_num = 1;
	//sop.sem_op = -1;
	//sop.sem_flg = 0;
	//if (semop(semid, &sop, 1) == -1)
	//errExit("semop");
	

	// incrementar el semaforo 1 cuando sale
	//sop.sem_num = 1;
	//sop.sem_op = 1;
	//sop.sem_flg = 0;
	//if (semop(semid, &sop, 1) == -1)
	//errExit("semop");
	return 0;
}
