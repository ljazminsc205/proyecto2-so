
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <time.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string.h>
#include <strings.h>

#include "svshm_string.h"
#include "mensaje.h"

struct datosProceso
{
    int prioridad;
    int burst;
    pthread_t tid;
};

struct proceso{
    int sockfd;
    char type[3];
};

struct proceso listaP[3]= {{1,"r"},{2,"s"},{3,"w"}};

int socket_desc, client_sock, c, read_size;
struct sockaddr_in server, client;
char client_message[2000]; 
pthread_t tmcid;
u_int16_t PUERTO_SERVIDOR;
pthread_t tlisten;
pthread_t recMesagge;

void all() {
	int shmid, size_shmid, semid;
	struct Mensaje *lista;
	int *size;
	struct sembuf p[1] ;//= {0, -1, 0};
    p[0].sem_num = 0;        /* Operate on semaphore 0 */
    p[0].sem_op = -1;         /* Increment value by one */
    p[0].sem_flg = 0;
	struct sembuf v[1] ;
	v[0].sem_num = 0;        /* Operate on semaphore 0 */
	v[0].sem_op = +1;         /* Decrement value by one */
	v[0].sem_flg = 0;

	/* consigue el id de la memoria compartida 
	 * con permisos de read y write */
	shmid = shmget(KEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");

	// asigna lista a la memoria compartida
	lista = (struct Mensaje *)shmat(shmid, NULL, 0);
	if (lista == (struct Mensaje *) -1)
	errExit("shmat");

	// obtener el tamano de la lista 
	size_shmid = shmget(SKEY, MEM_SIZE, 0666);
	if (shmid == -1)
	errExit("shmget");
	
	size = shmat(size_shmid, NULL, 0);
	if (size == (void *) -1)
	errExit("shmat");

	// obtener el id del semaforo
	semid = semget(SEM_KEY, 1, 0666);
    if (shmid == -1)
    errExit("shmget");
	
	 /* Decrement semaphore to 0 */
    if (semop(semid, p, 1) == -1)
    	errExit("semop");
	// mostrar la lista 
	int i;
	for (i=0; i<*size; i++) {
		printf("%i ", lista[i].key);
		printf("data: %.*s\n", (int) sizeof(lista[i].data), lista[i].data);
		//sleep(1);
	}
	/* Increment semaphore to 0 */
	 if (semop(semid, v, 1) == -1)
	 	errExit("semop");
}

void crearSocket()
{
	//crear el socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1)
	{
		printf("No fue posible crear el socket");
	}
	puts("--");
    //preparar la estructura de sockaddr_in
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PUERTO_SERVIDOR);
    //bind
	if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		perror("el bind fallo");
	}
	puts(" ");
}

int recibirMensaje(int socketCliente)
{
	//int socketCliente = *(int *)ptrCliente;
	//free(ptrCliente);
	char message[2000];
	int cantRecv = 0;
	while ((read_size = recv(socketCliente, message, 2000, 0)) > 0)
	{
		cantRecv += read_size;
		//actualizar la lista con el mensaje
		char val[2000];
		strcpy(val, message);  
		if (strcmp(val, "w") == 0)
		{ 
			listaP[2].sockfd=socketCliente;
			printf("Sockfd  W :%i Type  %s\n",listaP[2].sockfd , listaP[2].type);
		} 
		if (strcmp(val, "s") == 0)
		{  
			listaP[1].sockfd=socketCliente;
			printf("Sockfd  S: %i Type  %s\n",listaP[1].sockfd , listaP[1].type);
		} 
		if (strcmp(val, "r") == 0)
		{
			listaP[0].sockfd=socketCliente;
			printf("Sockfd R: %i Type  %s\n",listaP[0].sockfd , listaP[0].type);
		} 
		if(cantRecv >= 2000) break;
	}
	if (read_size == 0)
	{
		puts("Cliente desconectado");
		fflush(stdout);
		close(socketCliente);
		return 0;
	}
	else if (read_size == -1)
	{
		perror("fallo recv");
	}
	return 0;
}

void *acceptClients(void *arg)
{
	crearSocket(); 
	listen(socket_desc, 3);
	puts("Esperando procesos entrantes ....");
	c = sizeof(struct sockaddr_in);
	int nuevoCliente;
	while (1)
	{
		//aceptar conexion de un cliente
		nuevoCliente = accept(socket_desc, (struct sockaddr *)&client, (socklen_t *)&c);
		if (nuevoCliente < 0)
		{
			perror("fallo en aceptar el cliente");
			continue;
		}
		puts("Procesos llegando...");
		int *clientePtr = malloc(sizeof(int));
		*clientePtr = nuevoCliente;
        printf("Nuevo cliente: %i\n" , nuevoCliente);
		recibirMensaje(nuevoCliente);
	}
	return 0;
};

int recibirProcesos(int socketCliente)
{
	
	char message[20000];
	bzero(message, 20000);
	while ((read_size = recv(socketCliente, message, 20000, 0)) > 0)
	{
		printf("%s\n" , message);
		break;
		//write(socketCliente, message, strlen(message));
	}
	if (read_size == 0)
	{
		puts("Cliente desconectado");
		fflush(stdout);
		close(socketCliente);
		return 0;
	}
	else if (read_size == -1)
	{
		perror("fallo recv");
	}
	return 0;
}

void * listenSpy(){   
    char opc;
    while (1)
    {
        printf ("\nMenu\na = estado del archivo\nr = estado de los readers\ns = estado de los readers egoistas\nw = estado de los writters\n>> ");
        scanf("%s",&opc); 
        if (opc == 'a')
        {
			all();
            //printf("Opcion: %c\n", opc);
        }else if (opc =='r')
        {
			char message[2000]="\nNecesito los procesos readers";
			write(listaP[0].sockfd, message, 2000);
			recibirProcesos(listaP[0].sockfd);

        }else if ( opc=='s')
        {
            char message[2000]="\nNecesito los procesos readers egoistas";
			write(listaP[1].sockfd, message, 2000);
			recibirProcesos(listaP[1].sockfd);
            //printf("Opcion: %c\n", opc);

        }else if (opc =='w')
        {
            char message[2000]="\nNecesito los procesos writers";
			write(listaP[2].sockfd, message, 2000);
			recibirProcesos(listaP[2].sockfd);
            //printf("Opcion: %c\n", opc);
        }else
        {
            printf("Parametro ingresado %s\n" , "no válido" );
        }
    }
    
};

int main(int argc, char const *argv[])
{
 	PUERTO_SERVIDOR = 6466;	
	pthread_create(&tmcid, NULL, acceptClients, NULL);
	//listenSpy();
    pthread_create(&tlisten, NULL, listenSpy, NULL);
	pthread_join(tlisten, NULL);

};