#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <strings.h>
#include "stackProcess.c" 
#include "svshm_string.h"
#include "mensaje.h"
 
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdbool.h>

struct PCB *procesosReaders = NULL;
int contPID = 100;
int sockfd = 0;
int contReaders = 0;
u_int16_t PUERTO_SERVIDOR;
int  read_size;
pthread_t recMesagge;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void * recibirMensaje()
{
    int bRead = 0;
    int bSend = 0;
	char msj[2000] ;
    int cantRecv = 0;
    while (1)
    {
        bzero(msj, 2000);
        while( (bRead = recv(sockfd, msj, 2000, 0)) > 0 ) {
            //printf("Mensaje recibido del server(%i): %s\n " ,bRead, msj);
            char * msj = concatProcess(procesosReaders);
            //printf("%s\n\n", msj); 
            bSend = 0;
            char mensj[20000];
            strcpy(mensj, msj);
            bSend = send(sockfd, mensj, 20000, 0);
            if(cantRecv >= 2000) break;
        } 
   }
};
   
int createSocket(){
    PUERTO_SERVIDOR = 6466;
    struct sockaddr_in serv_addr;

    /* socket() */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        return 1;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PUERTO_SERVIDOR);

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1)
    {
        perror("connect : ");
        printf("fail to connect.\n");
        close(sockfd);
        return 1;
    }
//Mensaje readers->spy
    int bRead = 0;
    char mensj[2000] = "r"; 
    bRead = send(sockfd, mensj, 2000, 0);
    if (bRead <= 0)
    {
        printf("No se pudo enviar la instruccion de obtencion de archivo al servidor\n");
        exit(1);
    }
    else
    {
        printf("Instruccion enviada\n");
        pthread_create(&recMesagge, NULL, recibirMensaje,  NULL);
    } 
    return 0;
};

void *funcReader  (void * mensaje){
    struct PCB * datos = (struct PCB *) mensaje;
    int t_sleep = datos->t_sleep;
    int t_work = datos->t_work;
    int PID = datos->pid;
    int type = datos->type;
    int state = datos->state;
    int ifile = datos->ifile;
    int shmid, size_shmid, semid;
    struct Mensaje *lista;
    int *size;
    struct sembuf p[1] ;//= {0, -1, 0};
    p[0].sem_num = 0;        /* Operate on semaphore 0 */
    p[0].sem_op = -1;         /* Increment value by one */
    p[0].sem_flg = 0;
	struct sembuf v[1] ;
	v[0].sem_num = 0;        /* Operate on semaphore 0 */
	v[0].sem_op = +1;         /* Decrement value by one */
	v[0].sem_flg = 0;

    shmid = shmget(KEY, MEM_SIZE, 0666);
    if (shmid == -1)
    errExit("shmget");
    // asigna lista a la memoria compartida
    lista = (struct Mensaje *)shmat(shmid, NULL, 0);
    if (lista == (struct Mensaje *) -1)
    errExit("shmat");
    // obtener el tamano de la lista 
    size_shmid = shmget(SKEY, MEM_SIZE, 0666);
    if (shmid == -1)
    errExit("shmget");

    size = shmat(size_shmid, NULL, 0);
    if (size == (void *) -1)
    errExit("shmat");
    // obtener el id del semaforo
    semid = semget(SEM_KEY, 1, 0666);
    if (shmid == -1)
    errExit("shmget");

    while (1)
    {
        pthread_mutex_lock(&lock);
            contReaders++;
            if (contReaders==1)
            {
            /* Decrement semaphore to 0 */
            if (semop(semid, p, 1) == -1)
                errExit("semop");
            }    
        pthread_mutex_unlock(&lock);

        updateProcess(PID , ifile, 1, procesosReaders );
        state=1;

        if(strcmp(lista[ifile].data, "") != 0)
        {
            printf("PID: %i | Type: %i | State: %i | Work: %i | Sleep: %i | ifile: %i ", PID,type,state,t_work,t_sleep,ifile);
            printf("| data: %.*s\n", (int) sizeof(lista[ifile].data), lista[ifile].data);
            sleep(t_work);
        //Escribe en Bitàcora
            time_t tiempo = time(0);
            struct tm *tlocal = localtime(&tiempo);
            char fecha[128];
            strftime(fecha,128,"Hora:%H:%M:%S Dìa:%d/%m/%y ",tlocal);

            char PID2[25];
            FILE* Bitacora;
            Bitacora=fopen("Bitacora.txt","at");
            sprintf(PID2,"%i",PID);
            fputs(PID2,Bitacora);
            fputs(", Proceso: Leer, Tipo: Reader, ",Bitacora);
            fputs(fecha,Bitacora);
            fputs("\n",Bitacora);
            fclose(Bitacora);

            ifile++; 
            if (ifile ==*size)
                ifile=0;
        }
        pthread_mutex_lock(&lock);
            contReaders--;
            if (contReaders==0)
            {
                /* Increment semaphore to 0 */
                if (semop(semid, v, 1) == -1)
                    errExit("semop");  
            }  
        pthread_mutex_unlock(&lock);
        updateProcess(PID , ifile, 0, procesosReaders );
        sleep(t_sleep);
        updateProcess(PID , ifile, -1, procesosReaders );
    }  
    return 0;
};

int main (int argc, char *argv[]){
    createSocket();
    int cant ;
    sscanf(argv[1], "%d", &cant);  
    printf("\nCant: %i", cant);

    int t_sleep;
    sscanf(argv[2], "%d", &t_sleep);  
    printf("\nSleep: %i", t_sleep);

    int t_work ;
    sscanf(argv[3], "%d", &t_work);  
    printf("\nWork: %i\n", t_work);

    pthread_t pid;
    for (size_t i = 0; i < cant; i++)
    {
        struct PCB *datosPCB = (struct PCB *) malloc(sizeof(struct PCB));
        datosPCB->pid= contPID;
        datosPCB->type = 0;
        datosPCB->ifile = 0;
        datosPCB->t_work=t_work;
        datosPCB->t_sleep=t_sleep;
        datosPCB->state = -1;
        pushProcessEnd(&procesosReaders, contPID , datosPCB->type  , datosPCB->ifile,  0, datosPCB->t_work ,  datosPCB->t_sleep);
        pthread_create(&pid, NULL, funcReader, (void *) datosPCB);
        contPID++;
    };
    //printProcess(procesosReaders); 
    pthread_join(pid, NULL);
    return 0; 
    //
};
